class Algo:
    """This class decodes the assignment algorithm"""

    def __init__(self):
        self.result = []

    def fromfile(self, f):
        """decode directly from bytes from an opened file
            (without fear to colapse the memory)
            appending decoded to existing result
            TODO: use a buffer to speed up
        """

        pbyte = f.read(1)
        while pbyte != "":
            qbyte = f.read(1)

            #processing the duple
            self.decode(pbyte, qbyte)

            #next iteration
            pbyte = f.read(1)


    def decode(self, p,q):
        """decode a couple of p,q values and append it to result"""

        ordp = ord(p)

        #if p=0 only appends q to the result
        if ordp==0:
            self.result.append(q)
            return

        ordq = ord(q)
        #invalid or incomplete
        if ordq>ordp or ordp>len(self.result):
            self.result.append(b'\x3F')
            return

        #else splice from the result itself (strange!!)
        istart = len(self.result) - ordp
        iend = istart + ordq
        self.result = self.result + self.result[istart:iend]

    def dump(self):
        """outputs result in a string"""
        return ''.join(chr(x) for x in self.result)

