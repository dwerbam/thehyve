import unittest
from Algo import *

class TestAlgo(unittest.TestCase):

    def setUp(self):
        self.algo = Algo()

    def test_decode(self):
        self.algo.decode(b'\x00', b'\x20')
        self.assertEqual(len(self.algo.result), 1)
        self.assertTrue(self.algo.result[0] == b'\x20')

    def test_decodeTwo(self):
        self.algo.decode(b'\x00', b'\x20')
        self.algo.decode(b'\x01', b'\x01')

        self.assertEqual(len(self.algo.result), 2)
        self.assertTrue(self.algo.result[0] == b'\x20')
        self.assertTrue(self.algo.result[1] == b'\x20')


    def test_fromfile(self,):
        self.assertTrue(True)
        #...test here the stdin big files

if __name__ == '__main__':
    unittest.main()